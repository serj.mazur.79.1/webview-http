using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScene : MonoBehaviour
{
    public Slider s;
    public GameObject but;
    public float t = 0;

    void Start()
    {
        s.transform.localScale = new Vector3(0, 0, 0);
        s.value = t;
        but.transform.localScale = new Vector3(0, 0, 0);
        if (!PlayerPrefs.HasKey("lead"))
        {
            StartCoroutine(SendRequest());
        }
        else
        {
            if (PlayerPrefs.GetInt("lead") == 0)
            {
                GoToMenu();
            }
            else
            {
                OpenWebView();
            }
        }
    }

    void Update()
    {
        if(s.transform.localScale.x == 1 && t < 5 && but.transform.localScale.x == 0)
        {
            t += Time.deltaTime;
            s.value = t;
        }
        else if (s.transform.localScale.x == 1 && t >= 5 && but.transform.localScale.x == 0)
        {
            but.transform.localScale = new Vector3(1, 1, 1);
            s.value = t;
        }
    }

    private IEnumerator SendRequest()
    {
        UnityWebRequest request = UnityWebRequest.Get("https://giveus.party/blacktest");
        yield return request.SendWebRequest();

        if (request.responseCode == 200 && !string.IsNullOrWhiteSpace(request.downloadHandler.text) && !string.IsNullOrEmpty(request.downloadHandler.text))
        {
            PlayerPrefs.SetInt("lead", 1);
            PlayerPrefs.SetString("endpoint", request.downloadHandler.text);

            OpenWebView();
        }
        else
        {
            PlayerPrefs.SetInt("lead", 0);
            GoToMenu();
        }
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void OpenWebView()
    {
        var webView = gameObject.AddComponent<UniWebView>();
        webView.Frame = new Rect(0, 100, Screen.width, Screen.height - 100);

        webView.Load(PlayerPrefs.GetString("endpoint"));

        webView.Show();
        s.transform.localScale = new Vector3(1, 1, 1);
    }
}
